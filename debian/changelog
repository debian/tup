tup (0.8-1) unstable; urgency=medium

  * Qa upload.
  * New upstream version 0.8
  * Drop pcre2.patch: upstream
  * Drop add-support-for-s390x.patch: upstream
  * drop add-support-riscv64.patch: upstream
  * Update copyright years
  * Update upstream website to https version
  * Bump compat level to 13
  * Bump std-version to 4.7.0
  * Refresh patches for new release
  * watch file in secure mode
  * Update pkg-config to pkgconf
  * Add myself to copyright, to please lintian
  * Update fuse to fuse3 (Closes: #1084407)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Thu, 10 Oct 2024 16:00:52 +0200

tup (0.7.11-6) unstable; urgency=medium

  [ helmut ]
  * Drop the sysctl call to make it installable again after
    sysctl.conf was dropped (unprivileged-clone is now default).

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Thu, 10 Oct 2024 15:02:09 +0200

tup (0.7.11-5) unstable; urgency=medium

  * Team upload.
  [ zhangdandan <zhangdandan@loongson.cn> ]
  * Add support for loong64 (Closes: #1068685)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Fri, 27 Sep 2024 16:13:06 +0200

tup (0.7.11-4) unstable; urgency=medium

  * QA upload.
  * Drop emacs integration which fails the build.
  * Build with pcre2. (Closes: #999937)

  [ Helge Deller ]
  * Fix ftbfs on hppa. (Closes: #1030553)

 -- Bastian Germann <bage@debian.org>  Fri, 22 Dec 2023 01:11:31 +0100

tup (0.7.11-3) unstable; urgency=medium

  * QA upload.
  * Fix ftbfs on riscv64. (Closes: #1022175)

 -- Bo YU <tsu.yubo@gmail.com>  Tue, 31 Jan 2023 22:50:35 +0800

tup (0.7.11-2) unstable; urgency=medium

  * QA upload.

  [ Lucas Kanashiro ]
  * d/p/add-support-for-s390x.patch: allow builds on s390x.
  * d/tests/files/Tupfile: call cc instead of cp to actually compile the code.
    Right now, it copies the hello.c to hello and tries to execute it which
    obviously does not work.

 -- Bastian Germann <bage@debian.org>  Wed, 14 Dec 2022 11:22:01 +0100

tup (0.7.11-1) unstable; urgency=medium

  * QA upload.
  * New upstream release
  * Adjust patch "Do not assume git checkout"
  * Refresh patches
  * Add build dependency on FUSE3 library

 -- Fukui Daichi <a.dog.will.talk@akane.waseda.jp>  Mon, 07 Mar 2022 14:45:18 +0000

tup (0.7.8-3) unstable; urgency=medium

  * Highlight `preload' keyword in vim syntax file.

 -- Dmitry Bogatov <KAction@debian.org>  Tue, 19 Feb 2019 08:34:28 +0000

tup (0.7.8-2) unstable; urgency=medium

  * Update maintainer email.
  * Build-depend on `debhelper-compat' (obsoletes `debian/compat')
  * Update Vcs-* fields in debian/control.

 -- Dmitry Bogatov <KAction@debian.org>  Sat, 24 Nov 2018 22:13:29 +0000

tup (0.7.8-1) unstable; urgency=medium

  * New upstream release
  * Refresh patches
  * Update standards version to 4.2.1
  * wrap-and-sort -sta
  * Add build dependency on PCRE library
  * Adjust patch "Do not assume git checkout" to changes in build system
  * Add GitLab CI configuration file
  * Add autopkgtest to check

 -- Dmitry Bogatov <KAction@gnu.org>  Fri, 28 Sep 2018 13:24:55 +0300

tup (0.7.6-2) unstable; urgency=medium

  * Add dependency on bin:procps, which provides /sbin/sysctl, used in
    postinst maintainer script.

 -- Dmitry Bogatov <KAction@gnu.org>  Fri, 08 Jun 2018 23:27:41 +0300

tup (0.7.6-1) unstable; urgency=low

  * Initial release (Closes: #898980)

 -- Dmitry Bogatov <KAction@gnu.org>  Sat, 12 May 2018 21:12:03 +0300
